# title page

# About the Authors
* Catherine (Kate) Starbird
  - Adjunct faculty member at the Paul G. Allen School of Computer Science & Engineering and the Information School at University of Washington
  - Co-founder of the Center for an Informed Public (CIP)
  - PhD Technology, Media, & Society, University of Colorado Boulder (2012)
  - BS Computer Science, Stanford University (1997)
  - Played professional basketball in the American Basketball League, WNBA, and European teams (1997-2006)
* Emma S. Spiro
  - Adjunct Assistant Professor in the Department of Sociology & affiliate of the UW Center for Statistics and the Social Sciences
  - Co-founder of the Center for an Informed Public (CIP)
  - PhD Sociology, University of California, Irvine (2013)
  - MA Institute for Mathematical Behavioral Sciences, University of California, Irvine (2011)
  - BA Applied Mathematics & BA Science, Technology, and Society,  Pomona College (2007)
* Isabelle Edwards
* Kaitlyn  Zhou
* Jim  Maddock
* Sindhuja  Narasimhan

# Background:
* Rumoring during Crisis events
  - Definition of rumor is still up for debate.
  -  many conceptualize rumoring as a social process whereby information spreads in a population
  - Lists how others have redefined the term rumor: (1) rumors are a tool for collective problem-solving when groups make sense of the uncertainties of their environment [Shibutani] (2) public communications that reflect individual belief [Rosnow]
  - Rumors are also closely related to the concepts of "credibility" and evidence. E.g. (1) rumors as propositions that pass from one person to the next without standards of supporting evidence; (2) rumors represent conclusions based on unverified information that attempt to make sense of uncertain situations
  - summarizing definition list goes on
* Uncertainty in Rumoring during Crises
  - Studies show that uncertainty in the information environment contributes to rumoring
  - “uncertainty exists when details of situations are ambiguous, complex, unpredictable, or probabilistic; when information is unavailable or inconsistent; and when people feel insecure in their own state of knowledge or the state of knowledge in general” (Brashers, Brabow, et al)
  - uncertainty = danger, people actively engage in information seeking == reducing uncertainty from inconsistent or contradictory sources
  - uncertainty == crowd communication and “sense-making” process == “an ongoing process of making sense of the circumstances in which people collectively find [themselves] and of the events that affect them.” (Weick & Sutcliffe)
  - Internet rumors (Bordia & DiFonza) but does not focus on uncertainty.
  - Starbird et al. and Maddock et al. examined the spread of misinformation online in the aftermath the Boston Marathon Bombings. Coding for uncertainty was difficult perhaps bc of ambiguity of what is uncertain

# Themes and RQs
* This paper and (from what I've heard) from the general research approach that Dr. Starbird takes is that these questions don't come up until well into the process of collecting, coding, and looking at the data.
* This is a bit different than what we are usually taught, which is come up with a question and/or hypothesis and test it.
* Instead, this study, like our current mask study, approaches the data with open ended questions and as the researchers dived into the data and got a better sense of what what happening, did the specific research questions come up. Such as the phrases, or linguistic patterns of the expressed uncertainty, or if there was a way to define what these patterns mean and how they contributed to sensemaking (i guess)

# Methods
* Events & Data collection: 6 rumors from 2 crisis events. Boston Marathon Bombings and the Sydney Siege
  - Twitter Streaming API to track several event-related terms
  - Collected data from April 15 - April 21 for the Boston Marathon & December 15 + 2 weeks after
  -  Only for the Boston Marathon collection was data collecting rate-limited at 50 tweets per second, and we experienced two brief outages where no data was Collected
  - total 10.6 million tweets (Boston) adn 5.4 million (Sydney)
* Coding Tweets for Affirm/Deny & Uncertainty
  - manually code tweets associated with each rumor along two dimensions
  - Dimension 1:  identify crowd corrections, consists of five mutually exclusive categories: Affirm, Deny, Neutral, Unrelated, and Uncodable
  - Dimension 2:  captures expressed uncertainty—tweet text that suggests in some way that the veracity of the rumor is not completely established
  * 3 trained coders
  * Arbitration:  use a “majority rules” process for adjudication where agreement by two or more coders determines the final code
* Developing a Coding Scheme for Uncertainty
  - inductive and deductive methods we iteratively developed a coding scheme for uncertainty
  - grouped tweets according to perceived similarities, including linguistic patterns, grammatical constructs, and punctuation choices which were categorized into 5 groups: questions, hedges, deflections, approximators, and shields. Further expanded their coding groupings to include milling based on previous related studies
* Coding Scheme for Characterizing Expressed Uncertainty in Rumoring Tweets
  - consisted of 2 high level rumor-behavior categories: shielding & milling
  - Shielding is further classified into Attributions and Plausibility
  - Milling is further classified into Building / Doubting; Open question; Emotional sensemaking; and each of those are further subcategorized
* Coding Tweets for Types of Expressed Uncertainty
  - First, coders assessed whether the uncertainty in the tweet was Related or Unrelated to the rumor story
  - Throughout coding researchers also kept track of specific phrases, words, punctuation or grammatical patterns associated with each code. This made it i easier to see similarities between different categories and when the same phrase expressed different types of uncertainty.
* Analysis
  - first graphed related uncertainty as it relates to affirm, deny and neutral signal
  - We divided counts into original tweets and retweets, calculating retweet percentage as a proportion of the total. This way we could see when spikes were caused primarily by retweets or original tweets

# Results
* describe and analyze tweets related to the six rumors selected for this study
* describe each rumor, 3 for the 2013 Boston Marathon Bombings and 3 for the 2014 Sydney Siege event
* Rumor #1: claimed that a man was planning to propose to his girlfriend at the finish line, but that she had been injured or killed by the blasts
* Specific phrases and linguistic patterns that appear in rumor-related posts that contain uncertainty
* Conditional statements such as these were a prominent pattern in milling behavior, both building and doubting, appearing in 15.3% of tweets across all rumors.

# Contributions
*  By applying this coding scheme to a large corpus of tweets we identified specific words and linguistic patterns that are characteristic of different types of rumoring behavior.

# Arguments
* linguistic shields—i.e. mechanisms that protect the author if a rumor turns out to be false. In 4 of 6 rumors, these were primarily impersonal plausibility shields, often communicated through words like "possibly" or "could be"
* Attribution shields, which deflect the responsibility of a rumor onto another source was also used
* Verbal milling - behavior during crisis events as a process of collective sensemaking through which people provide and discuss possible explanations what has occurred (60% of tweets in dataset showed this)
* Leading Questions to Build/Challenge Rumors -  34% of all uncertainty tweets were formed in leading questions. Leading questions are phrased in a biased way, showing a lack of neutrality that we would see in an open question
* Leading Questions as Impersonal Plausibility Shields - a method for spreading information (or doubting information) without fully committing to those claims. 73% of milling leading questions were also coded as impersonal plausibility shields
  - hypothesize that people phrase claims in a question format both as an expression of some doubt and as a mechanism for avoiding blame if the theory they are putting forward is later proven false
* They even covered temporal patterns in uncertainty - Shifts in the type of uncertainty, for example from milling/building to attribution shields, may also align with significant moments in a rumor’s evolution
and characterize certain types of rumors.

# Implications / Future work
* Using Expressed Uncertainty to Detect Rumors
  - leveraging collective sensemaking processes, specifically crowd corrections and skepticism to build automated rumor detection systems
  - expressed uncertainty may be an earlier indicator of rumors than denials or corrections, which could improve speed of detection
  - Thinking about using specific kinds of expressed uncertainty, e.g. attribution and plausibility shields and milling/building (those that accompany the affirming phases of some rumors) used as early predictors of early predictors of rumoring
* Using Expressed Uncertainty to Understand Sensemaking
  - Goal of this study is to better understand collective sensemaking online
  - focused on coding posts centered around a crisis but the authors see that this coding scheme could be applied to other kinds of online discussions
  -  intend to expand this coding scheme in future work to address a more complete range of sensemaking activities, including posts without uncertainty
